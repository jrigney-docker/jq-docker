# jq-docker
jq(https://stedolan.github.io/jq/) in a docker container

## build
docker build -t jrigney/jq .

## test run
curl 'https://api.github.com/repos/stedolan/jq/commits?per_page=5' | docker run -i jrigney/jq '.'

## jq manual
https://stedolan.github.io/jq/manual/

