FROM alpine:3.5

RUN apk update \
  && apk add --no-cache --virtual \
      jq

ENTRYPOINT ["jq"]
CMD ["-h"]
